# Py Security

## Notes
This is coursework for CSE 627.

## Set up 
1. Create a new virtual environment
```
python3.6 -m venv venv
```
2. Activate the virtual environment
```
venv\Scripts\activate
```
3. Install the dependencies
```
pip install -r requirements.txt
```

## Running the application
To launch the GUI use the command
```
python main.py
```
The GUI looks like this:
![GUI Screenshot](imgs/gui.png)

To use the package as part of another script:
```python
>>> from pysecurity import Ciphers as cs
>>> cs.encrypt(cipher="caesar", key=1, text="abcd")
'bcde'
```

## Packaging the application
1. Install `pyinstaller`
```
pip install pyinstaller
```
2. Package the application into a single executable
```
pyinstaller main.py -n pysecurity --onefile
```
If you'd like the final executable file to not have a console, add the option `--noconsole`
```
pyinstaller main.py -n pysecurity --onefile --noconsole
```

The final executable will be available under `dist` folder.
