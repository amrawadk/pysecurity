import logging
from collections import namedtuple
from pysecurity import Ciphers as cs

logging.basicConfig(format="%(levelname)s - %(message)s", level=logging.DEBUG)
logger = logging.getLogger("ciphers")

example = namedtuple("Example", ["input_path", "output_path", "cipher", "key"])

EXAMPLES = [
    example(
        input_path="input_files/Caesar/caesar_plain.txt",
        output_path="output_files/Caesar/caesar_plain_key_3.txt",
        cipher="caesar",
        key=3,
    ),
    example(
        input_path="input_files/Caesar/caesar_plain.txt",
        output_path="output_files/Caesar/caesar_plain_key_6.txt",
        cipher="caesar",
        key=6,
    ),
    example(
        input_path="input_files/Caesar/caesar_plain.txt",
        output_path="output_files/Caesar/caesar_plain_key_12.txt",
        cipher="caesar",
        key=12,
    ),
    example(
        input_path="input_files/Hill/hill_plain_2x2.txt",
        output_path="output_files/Hill/hill_plain_2x2.txt",
        cipher="hill",
        # 5, 17, 8, 3
        key="frid",
    ),
    example(
        input_path="input_files/Hill/hill_plain_3x3.txt",
        output_path="output_files/Hill/hill_plain_3x3.txt",
        cipher="hill",
        # [2, 4, 12, 9, 1, 6, 7, 5, 3]
        key="cemjbghfd",
    ),
    example(
        input_path="input_files/PlayFair/playfair_plain.txt",
        output_path="output_files/PlayFair/playfair_rats.txt",
        cipher="playfair",
        key="rats",
    ),
    example(
        input_path="input_files/PlayFair/playfair_plain.txt",
        output_path="output_files/PlayFair/playfair_archangel.txt",
        cipher="playfair",
        key="archangel",
    ),
    example(
        input_path="input_files/Vernam/vernam_plain.txt",
        output_path="output_files/Vernam/vernam_ciphertext.txt",
        cipher="vernam",
        key="SPARTANS",
    ),
    example(
        input_path="input_files/Vigenere/vigenere_plain.txt",
        output_path="output_files/Vigenere/vigenere_pie_repeat.txt",
        cipher="vigenere",
        key="pie",
    ),
    example(
        input_path="input_files/Vigenere/vigenere_plain.txt",
        output_path="output_files/Vigenere/vigenere_aether_auto.txt",
        cipher="vigenere",
        key="aether",
    ),
]

if __name__ == "__main__":
    for ex in EXAMPLES:
        with open(ex.input_path, "r") as fh:
            lines = fh.readlines()

        output_lines = list()
        for line in lines:
            try:
                if ex.key == "aether":
                    output_lines.append(cs.encrypt(cipher=ex.cipher, text=line.strip(), key=ex.key, auto=True))
                else:
                    output_lines.append(cs.encrypt(cipher=ex.cipher, text=line.strip(), key=ex.key))
            except Exception:
                logger.exception("Encryption Failed")

        with open(ex.output_path, "w") as fh:
            fh.write("\n".join(output_lines))
