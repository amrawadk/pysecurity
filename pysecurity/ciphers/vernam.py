import logging

logger = logging.getLogger("ciphers")


def vernam_encrypt(text, key):
    if type(key) == int:
        key = str(key)

    if len(text) != len(key):
        raise ValueError("Text must have same length as key for a one-time pad cipher")

    encrypted_text = "".join(chr(ord(a) ^ ord(b)) for a, b in zip(text, key))
    logger.debug("Encryption completed, result: %s", encrypted_text)
    return encrypted_text


if __name__ == "__main__":
    vernam_encrypt(text="hitheree", key="arcadser")
