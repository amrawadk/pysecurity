import math
import string
import logging
import numpy as np

logger = logging.getLogger("ciphers")
mod_26 = lambda x: x % 26


def vigenere_encrypt(text, key, auto=False):
    logger.debug("Original Key: %s", key)
    if type(key) == int:
        key = str(key)

    key = key.lower()

    if auto:
        key = (key + text)[: len(text)]
        logger.debug("Auto mode enabled, concatinating key + message, result: %s", key)
    else:
        key = (key * math.ceil(len(text) / len(key)))[: len(text)]
        logger.debug("Auto mode disabled, repeating key, result: %s", key)

    text = [string.ascii_lowercase.find(c) for c in text]
    logger.debug("mapped text: %s", list(text))

    key = [string.ascii_lowercase.find(c) for c in key]
    logger.debug("mapped key: %s", list(key))
    encrypted_array = mod_26(np.array(key) + np.array(text))
    logger.debug("encrypted array: %s", encrypted_array.tolist())

    encrypted_text = "".join([string.ascii_lowercase[i] for i in encrypted_array])
    logger.debug("Encryption done, result: %s", encrypted_text)

    return encrypted_text


if __name__ == "__main__":
    vigenere_encrypt(text="hitheree", key="arca")
