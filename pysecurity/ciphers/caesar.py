import string
import logging

logger = logging.getLogger("ciphers")


def caesar_encrypt(text, key):
    try:
        key = int(key)
    except ValueError:
        raise ValueError("Caesar key must be an integer")

    cipher = string.ascii_lowercase[key % 26 :] + string.ascii_lowercase[: key % 26]

    mapping = {
        org_char: encoded_char
        for org_char, encoded_char in zip(string.ascii_lowercase, cipher)
    }

    logger.debug("Generated Mapping:")
    logger.debug("Input:  %s", list(mapping.keys()))
    logger.debug("Output: %s", list(mapping.values()))

    encrypted_list = [mapping[char] if char in mapping else char for char in text]
    encrypted_text = "".join(encrypted_list)
    logger.debug("Encryption done, result: %s", encrypted_text)

    return encrypted_text


def caesar_decrypt(text, key):
    cipher = string.ascii_lowercase[key % 26 :] + string.ascii_lowercase[: key % 26]

    mapping = {
        encoded_char: org_char
        for org_char, encoded_char in zip(string.ascii_lowercase, cipher)
    }

    original_text = "".join([mapping[char] for char in text])
    return original_text


if __name__ == "__main__":
    print(caesar_decrypt(text=caesar_encrypt(text="abc", key=3), key=3))
