import string
from .caesar import caesar_encrypt
from .hill import hill_encrypt
from .playfair import playfair_encrypt
from .vigenere import vigenere_encrypt
from .vernam import vernam_encrypt


class Ciphers:
    SUPPORTED_ENCRYPTIONS = {
        "caesar": caesar_encrypt,
        "hill": hill_encrypt,
        "playfair": playfair_encrypt,
        "vigenere": vigenere_encrypt,
        "vernam": vernam_encrypt,
    }

    @classmethod
    def encrypt(cls, cipher, text, key, **kwargs):
        if cipher not in cls.SUPPORTED_ENCRYPTIONS.keys():
            raise ValueError(
                f"Cipher of type {cipher} not supported, supported ciphers are {list(cls.SUPPORTED_ENCRYPTIONS.keys())}"
            )

        return cls.SUPPORTED_ENCRYPTIONS[cipher](text=text, key=key, **kwargs)
