import re
import string
import logging
from collections import namedtuple


logger = logging.getLogger("ciphers")

Index = namedtuple("Index", ["col", "row"])


def generate_playfair_matrix(key):
    """Generates the Playfair matrix for mapping

    Arguments:
        key {string} -- A sequence of characters used to generate the matrix
    """
    if type(key) == int:
        key = str(key)

    # Remove duplicate characters from the key
    key = "".join(sorted(set(key), key=key.index))
    logger.debug("Filtered duplicates in key, result: %s", key)

    # construct the 5x5 table by using the key then the remaining characters
    # Also remove 'i' as it's a special case
    key = key.replace("i", "j")
    remaining_chars = re.sub(f"[i{key}]", "", string.ascii_lowercase)

    logger.debug("Generated mapping string: %s", key + remaining_chars)

    # Construct the playfair matrix
    logger.debug("Mapping matrix:")
    mapping = dict()
    for idx, char in enumerate(key + remaining_chars):
        mapping[char] = Index(col=idx // 5, row=idx % 5)

    mapping["i"] = mapping["j"]
    mapping_location = {val: key for key, val in mapping.items()}
    for row in range(5):
        chars = [mapping_location[(col, row)] for col in range(5)]
        logger.debug("\trow %s: %s", row, chars)

    logger.debug("Note: 'i' & 'j' have the same entry in the matrix")

    return mapping, mapping_location


def playfair_encrypt(text, key, filler_char="x"):
    logger.debug("Input plaintext: %s", text)
    logger.debug("Input key: %s", key)

    if any([c not in string.ascii_lowercase for c in text.lower()]):
        raise ValueError(f"Only asci letters are supported as input!")

    mapping, mapping_location = generate_playfair_matrix(key=key)

    # Repeating plaintext letters that are in the same pair are separated with a filler letter: X
    text = text.lower()
    updated_char_list = []
    for c1, c2 in zip(text[0:], text[1:]):
        updated_char_list.append(c1)
        if c1 == c2:
            updated_char_list.append(filler_char)
    updated_char_list.append(text[-1])

    text = "".join(updated_char_list)

    logger.debug(
        "Inserting %s between every two repeating letters, result: %s",
        filler_char,
        text,
    )

    # If there is a single trialing letter, attach filler letter to the end to complete
    # the two-letter block.
    if len(text) % 2 == 1:
        text += filler_char
        logger.debug(
            "text has odd char count, inserting %s at end, result: %s",
            filler_char,
            text,
        )

    # Start encoding
    logger.debug("Encrypting String ...")
    encrypted_list = []
    for c1, c2 in zip(text[0::2], text[1::2]):
        # 1. If both letters fall in the same row, replace each with letter to right (wrapping back
        # to start from end.)
        if mapping[c1].row == mapping[c2].row:
            encrypted_list.append(
                mapping_location[((mapping[c1].col + 1) % 5, mapping[c1].row)]
            )
            encrypted_list.append(
                mapping_location[((mapping[c2].col + 1) % 5, mapping[c2].row)]
            )
            logger.debug(
                "Letters %s, %s are in same row, encoded to: %s, %s",
                c1,
                c2,
                encrypted_list[-2],
                encrypted_list[-1],
            )
        # 2. If both letters fall in the same column, replace each with the letter below it
        # (wrapping to top from bottom.)
        elif mapping[c1].col == mapping[c2].col:
            encrypted_list.append(
                mapping_location[(mapping[c1].col, (mapping[c1].row + 1) % 5)]
            )
            encrypted_list.append(
                mapping_location[(mapping[c2].col, (mapping[c2].row + 1) % 5)]
            )
            logger.debug(
                "Letters %s, %s are in same column, encoded to: %s, %s",
                c1,
                c2,
                encrypted_list[-2],
                encrypted_list[-1],
            )
        # Otherwise, each letter is replaced by the letter in the same row and in the column
        # of the other letter of the pair.
        else:
            encrypted_list.append(mapping_location[(mapping[c1].col, mapping[c2].row)])
            encrypted_list.append(mapping_location[(mapping[c2].col, mapping[c1].row)])
            logger.debug(
                "Letters %s, %s handled normally, encoded to: %s, %s",
                c1,
                c2,
                encrypted_list[-2],
                encrypted_list[-1],
            )

    encrypted_text = "".join(encrypted_list)
    logger.debug("Encrytion completed, result: %s", encrypted_text)
    return encrypted_text


if __name__ == "__main__":
    playfair_encrypt(text="hitheree", key="arcac")
