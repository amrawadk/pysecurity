import string
import math
import numpy as np
import logging

logger = logging.getLogger("ciphers")

mod_26 = lambda x: x % 26


def hill_encrypt(text, key):
    if not math.sqrt(len(str(key))).is_integer():
        raise ValueError(f"Supplied key size {len(key)} doesn't have a square root!")

    if str(key) in ["0", "1"]:
        raise ValueError(f"Invalid key: {key}")
    dim = int(math.sqrt(len(key)))
    if len(text) % dim != 0:
        raise ValueError(
            f"Text length {len(text)} isn't divisible by the key matrix dimension {dim}!"
        )

    # Convert the text to numbers
    logger.debug("Original text: %s", list(text))
    text = [string.ascii_lowercase.find(c) for c in text.lower()]
    logger.debug("mapped text: %s", list(text))

    # Convert the key to numbers
    logger.debug("Original key: %s", list(key))
    key = [string.ascii_lowercase.find(c) for c in key.lower()]
    logger.debug("mapped key: %s", list(key))

    # Generate the key matrix
    key_m = np.array(key).reshape(dim, dim)

    encrypted_list = list()
    for chars in np.array_split(list(text), len(text) // dim):
        result = mod_26(np.dot(key_m, chars))
        encrypted_list.extend(result.tolist())

    encrypted_text = "".join([string.ascii_lowercase[i] for i in encrypted_list])
    logger.debug("Encryption done, result: %s", encrypted_text)

    return encrypted_text


if __name__ == "__main__":
    hill_encrypt(text="hitheree", key="arca")
