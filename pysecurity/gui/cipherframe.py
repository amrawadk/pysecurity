import tkinter as tk


class CipherFrame(tk.Frame):
    def __init__(self, master, cipher, encrypt):
        super().__init__(
            master,
            highlightbackground="black",
            highlightcolor="black",
            highlightthickness=1,
        )
        self.cipher = cipher
        self.encrypt = encrypt

        self.grid()
        self.key_value = tk.StringVar()
        self._create_widgets()
        self._adjust_layout()

    def _create_widgets(self):
        self.title = tk.Label(self, text=self.cipher, height=4, width=20)
        self.key_label = tk.Label(self, text="Key:", height=4, width=10)
        self.key_entry = tk.Entry(
            self,
            width=20,
            validate="focusout",
            validatecommand=self.encrypt,
            textvariable=self.key_value,
        )

        self.ciphertext_label = tk.Label(self, text="Cipher Text:", height=4)
        self.ciphertext_output = tk.Label(self, width=50, height=4)

    def _adjust_layout(self):
        self.title.grid(row=1, column=1, padx=20)
        self.key_label.grid(row=1, column=2, padx=20)
        self.key_entry.grid(row=1, column=3, padx=20)

        self.ciphertext_label.grid(row=1, column=4, padx=20)
        self.ciphertext_output.grid(row=1, column=5, padx=20)
