import logging
import tkinter as tk
from .cipherframe import CipherFrame
from pysecurity.ciphers import Ciphers as cs


logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=logging.DEBUG
)


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.grid()

        self.logger = logging.getLogger("gui")
        self.logger.setLevel(logging.DEBUG)

        self.plaintext_value = tk.StringVar()

        self._create_widgets()
        self._configure_layout()
        self._initialize_key_values()

    def _initialize_key_values(self):
        self.logger.disabled = True
        self.caesar_cipher.key_value.set(1)
        self.hill_cipher.key_value.set("abcd")
        self.playfair_cipher.key_value.set("abc")
        self.vigenere_cipher.key_value.set("1")
        self.vernam_cipher.key_value.set("1")
        self.logger.disabled = False

    def _create_widgets(self):
        self.plaintext_label = tk.Label(self, text="Plain Text:", height=4)
        self.plaintext = tk.Entry(
            self,
            width=50,
            validate="focusout",
            validatecommand=self.encrypt,
            textvariable=self.plaintext_value,
        )

        self.caesar_cipher = CipherFrame(
            master=self,
            cipher="Caesar",
            encrypt=lambda: self.encrypt(ciphers=["caesar"]),
        )
        self.hill_cipher = CipherFrame(
            master=self, cipher="Hill", encrypt=lambda: self.encrypt(ciphers=["hill"])
        )
        self.playfair_cipher = CipherFrame(
            master=self,
            cipher="Playfair",
            encrypt=lambda: self.encrypt(ciphers=["playfair"]),
        )
        self.vigenere_cipher = CipherFrame(
            master=self,
            cipher="Vigenere",
            encrypt=lambda: self.encrypt(ciphers=["vigenere"]),
        )
        self.vernam_cipher = CipherFrame(
            master=self,
            cipher="Vernam",
            encrypt=lambda: self.encrypt(ciphers=["vernam"]),
        )

    def _configure_layout(self):
        self.plaintext_label.grid(row=0, column=0, padx=20, pady=10)
        self.plaintext.grid(row=0, column=1, padx=20, pady=10)
        self.caesar_cipher.grid(row=1, column=0, padx=20, pady=5, columnspan=2)
        self.hill_cipher.grid(row=2, column=0, padx=20, pady=5, columnspan=2)
        self.playfair_cipher.grid(row=3, column=0, padx=20, pady=5, columnspan=2)
        self.vigenere_cipher.grid(row=4, column=0, padx=20, pady=5, columnspan=2)
        self.vernam_cipher.grid(row=5, column=0, padx=20, pady=5, columnspan=2)

    def encrypt(self, **kwargs):
        if "ciphers" not in kwargs:
            ciphers = cs.SUPPORTED_ENCRYPTIONS.keys()
        else:
            ciphers = kwargs["ciphers"]

        for cipher in ciphers:
            frame = getattr(self, f"{cipher}_cipher")
            try:
                frame.ciphertext_output["text"] = cs.encrypt(
                    cipher=cipher,
                    text=self.plaintext_value.get(),
                    key=frame.key_value.get(),
                )
            except Exception as e:
                self.logger.error(f"Failed to encrypt message with cipher {cipher}")
                self.logger.error(f"Reason: {e}")
        return True

    @classmethod
    def run(cls):
        root = tk.Tk()
        app = cls(master=root)

        app.master.title("PySecurity")
        app.mainloop()
