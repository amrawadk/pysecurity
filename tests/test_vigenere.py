import pytest

from pysecurity import Ciphers as cs
from .factories import test


testdata = [
    test(
        plaintext="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",
        key="GERMAN",
        ciphertext="gftpesmlzvkysrfbqeyxlhwkedrncqkjxtiwqpdzocwvjfuicbpl",
    ),
    test(
        plaintext="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",
        key="CIPHERS",
        ciphertext="cjrkiwyjqyrpdfqxfywkmxemfdrteltmkyalsatrfhszhaymozgo",
    ),
]


@pytest.mark.parametrize("plaintext,key,ciphertext", testdata)
def test_caesar(plaintext, key, ciphertext):
    assert ciphertext == cs.encrypt(cipher="vigenere", text=plaintext, key=key)
