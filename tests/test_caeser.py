import pytest

from pysecurity import Ciphers as cs
from .factories import test


testdata = [
    test(
        plaintext="defend the east wall of the castle",
        key=1,
        ciphertext="efgfoe uif fbtu xbmm pg uif dbtumf",
    ),
    test(
        plaintext="attack at down on north west front",
        key=5,
        ciphertext="fyyfhp fy itbs ts stwym bjxy kwtsy",
    ),
    test(
        plaintext="visit the bank next morning",
        key=18,
        ciphertext="nakal lzw tsfc fwpl egjfafy",
    ),
    # Non-ascii charachters should not change
    test(
        plaintext='!@$%%^&*()_-+={}[]|":;<>,./?',
        key=9,
        ciphertext='!@$%%^&*()_-+={}[]|":;<>,./?',
    ),
]


@pytest.mark.parametrize("plaintext,key,ciphertext", testdata)
def test_caesar(plaintext, key, ciphertext):
    assert ciphertext == cs.encrypt(cipher="caesar", text=plaintext, key=key)
