import pytest

from pysecurity import Ciphers as cs
from .factories import test


testdata = [
    test(
        plaintext="abcdefghiklmnopqrstuvwxyzABCDEFGHIKLMNOPQRSTUVWXY",
        key="czbniwemuorkpdslqfxagthyv",
        ciphertext="finrmqtyzsfwiukfkryegoyniqnzkulhvbrqubmslkkvoyulny",
    ),
    test(
        plaintext="abcdefghiklmnopqrstuvwxyzABCDEFGHIKLMNOPQRSTUVWXY",
        key="ohnuwkdbtiepyzxcsqamlgrvf",
        ciphertext="qtskxlhdkdfcuhysgqztfuezavkqkplrwdeoqwhernadtuimze",
    ),
    test(
        plaintext="abcdefghiklmnopqrstuvwxyzABCDEFGHIKLMNOPQRSTUVWXY",
        key="pslqofaiymcnwtuhdxezbkgrv",
        ciphertext="fknhhybxagoiussokqucgueidmphxzibxfgsaupsyqqnzoxgie",
    ),
    test(
        plaintext="abcdefghiklmnopqrstuvwxyzABCDEFGHIKLMNOPQRSTUVWXY",
        key="xqvlpnakrsewfcyhutdbgziom",
        ciphertext="sudowcxgvfporgxvsndtqfpeqwdyhceitgrvgsmllakbtqeqep",
    ),
    test(
        plaintext="abcdefghiklmnopqrstuvwxyzABCDEFGHIKLMNOPQRSTUVWXY",
        key="xbhpuvmiyesdnwlakcrgfzotq",
        ciphertext="kxknvqcumcdechutawqpyspvfkhklmqaingdidthtgwfxespvp",
    ),
    test(plaintext="hithere", key="abcd", ciphertext="iksibucz"),
]


@pytest.mark.parametrize("plaintext,key,ciphertext", testdata)
def test_caesar(plaintext, key, ciphertext):
    assert ciphertext == cs.encrypt(cipher="playfair", text=plaintext, key=key)
