import pytest
from pysecurity import Ciphers as cs

from .factories import test


testdata = [
    test(
        plaintext="miss scarlet with the knife in the library",
        key="pftgpmiydgaxgoufhklllmhsqdqogtewbqfgyovuhwt",
        ciphertext="ANKYODKYUREPFJBYOJDSPLREYIUNOFDOIUERFPLUYTS",
        # actual result: Output is not as long as the input!
    ),
    test(
        plaintext="mr mustard with the candlestick in the hall",
        key="pxlmvmsydofuyrvzwc tnlebnecvgdupahfzzlmnyih",
        ciphertext="ANKYODKYUREPFJBYOJDSPLREYIUNOFDOIUERFPLUYTS",
        # actual result: '\x1d\nL\x00\x03\x1e\x07\x18\x16\x0bF\x02\x10\x06\x1eZ\x03\x0bET\r\r\x0b\x06\x02\x00\x10\x02\x0e\x07\x1eP\x08\x06F\x0e\x12\tM\x06\x18\x05\x04'
    ),
    test(
        plaintext="DCODEVERNAM",
        key="ENCRYPTIONS",
        ciphertext="HPQUCKXZBNE",
        # actual result: '\x01\r\x0c\x16\x1c\x06\x11\x1b\x01\x0f\x1e'
    ),
]


@pytest.mark.skip(
    reason="Unable to understand actual results, reasons added in each test case"
)
@pytest.mark.parametrize("plaintext,key,ciphertext", testdata)
def test_caesar(plaintext, key, ciphertext):
    assert ciphertext == cs.encrypt(cipher="vernam", text=plaintext, key=key)
