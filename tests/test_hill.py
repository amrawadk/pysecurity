import pytest

from pysecurity import Ciphers as cs
from .factories import test


testdata = [
    test(plaintext="ACT", key="GYBNQKURP", ciphertext="poh"),
    test(plaintext="GFG", key="HILLMAGIC", ciphertext="swk"),
]


@pytest.mark.parametrize("plaintext,key,ciphertext", testdata)
def test_caesar(plaintext, key, ciphertext):
    assert ciphertext == cs.encrypt(cipher="hill", text=plaintext, key=key)
